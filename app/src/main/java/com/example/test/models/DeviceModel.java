package com.example.test.models;

public class DeviceModel {
    String nameDevice;
    String macAdress;

    public DeviceModel(String nameDevice, String macAdress) {
        this.nameDevice = nameDevice;
        this.macAdress = macAdress;
    }

    public String getNameDevice() {
        return nameDevice;
    }

    public String getMacAdress() {
        return macAdress;
    }

    public void setNameDevice(String nameDevice) {
        this.nameDevice = nameDevice;
    }

    public void setMacAdress(String macAdress) {
        this.macAdress = macAdress;
    }
}
