package com.example.test.adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.test.R;
import com.example.test.models.DeviceModel;

import java.util.ArrayList;

public class devicesBlutoothAdaptor extends ArrayAdapter<DeviceModel> {

    public devicesBlutoothAdaptor(Context context, ArrayList<DeviceModel> devices) {
        super(context, 0, devices);
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        DeviceModel deviceModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_row, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.nameDevice);
        TextView tvHome = (TextView) convertView.findViewById(R.id.macAdress);
        // Populate the data into the template view using the data object
        tvName.setText(deviceModel.getNameDevice());
        tvHome.setText(deviceModel.getNameDevice());
        // Return the completed view to render on screen
        return convertView;
    }
}
